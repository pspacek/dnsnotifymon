#!/usr/bin/python
"""NOTIFY receiver for demonstration purposes"""
import argparse
import collections
import csv
import datetime
import ipaddress
import logging
import socket
import sys
from typing import Optional, Union

import dns.message
import dns.name
import dns.opcode
import dns.query
import dns.rdataclass
import dns.rdatatype
import dns.serial


IPAddr = Union[ipaddress.IPv4Address, ipaddress.IPv6Address]


class Notify:
    """
    NOTIFY message abstraction. Throws away everything except:
    - time received in UTC
    - source address
    - zone name
    - and optionally SOA serial (if it was present in the ANSWER section)
    """

    def __init__(self, time: datetime.datetime, src: IPAddr, msg: dns.message.Message):
        self.time = time
        self.src = src

        # TODO: check TSIG? do we care?
        if msg.opcode() != dns.opcode.NOTIFY:
            raise NotImplementedError("not a notify", msg)
        # Well, RFC 1996 sect 3.7 says that QDCOUNT could be > 1,
        # but I really do not want to deal with that
        self.check_section(msg, "question", require_rdata=False)
        self.zone_name = msg.question[0].name

        # RFC 1996 sect 3.7 says we might receive a new RRset:
        # if it contains just one serial use value from it
        # as optimization to avoid extra query
        # (BIND 9.18 attaches the SOA, for example)
        if len(msg.answer) == 0:
            self.serial = None
        try:
            self.check_section(msg, "answer", require_rdata=True)
        except NotImplementedError:
            # something we do not understand
            self.serial = None
            return
        self.serial = dns.serial.Serial(msg.answer[0][0].serial)

        # RFC 1996 sect 3.9: ignore authority and additional sections

    @staticmethod
    def check_section(msg, section_name: str, require_rdata: bool):
        """
        Section must have just one RRset of type SOA.
        If require_rdata == True requires exactly one RR in the RRset,
        otherwise no RRs are allowed.
        """
        section = getattr(msg, section_name)
        if len(section) != 1:  # only one RRset
            raise NotImplementedError(
                "not a single RRset in section", section_name, msg
            )
        rrset = section[0]
        # TODO: theoretically we could allow this
        # ... but I do not want to deal with overlaping namespaces
        if rrset.rdclass != dns.rdataclass.IN:
            raise NotImplementedError("rr class is not IN", section_name, msg)
        if rrset.rdtype != dns.rdatatype.SOA:
            raise NotImplementedError("rr type is not SOA", section_name, msg)
        if require_rdata and len(rrset) != 1:  # only one RR
            raise NotImplementedError("not a single RR in RRset", section_name, msg)

    def __str__(self):
        return f"NOTIFY from {self.src}: zone {self.zone_name} serial {self.serial}"


class Server:
    serial_last: dns.serial.Serial
    serial_seen: datetime.datetime
    notify_last: datetime.datetime
    refresh_requests: set[dns.name.Name]

    def __init__(self):
        self.refresh_requests = set()

    def record_notify(
        self,
        notify: Notify,
    ):
        self.notify_last = notify.time
        if notify.serial is not None:
            self.serial_last = notify.serial
            self.serial_seen = notify.time
            self.refresh_requests.discard(notify.zone_name)
        else:
            self.refresh_requests.add(notify.zone_name)


class Problem:
    addr: IPAddr


class Zone:
    highest_serial: Optional[dns.serial.Serial]
    servers: collections.defaultdict[IPAddr, Server]

    def __init__(self):
        self.servers = collections.defaultdict(Server)
        self.highest_serial = None

    def record_notify(self, notify: Notify):
        if notify.serial is not None:
            if self.highest_serial is None:
                self.highest_serial = notify.serial
            else:
                self.highest_serial = max(notify.serial, self.highest_serial)
        self.servers[notify.src].record_notify(notify)

    def check(self):
        for addr, server_state in self.servers.items():
            high = self.highest_serial
            my = server_state.serial_last
            if high is None:
                yield ("all", "no serial")
                continue
            if my is None:
                yield (addr, "no serial")
            elif my < high:
                yield (addr, f"server behind: {my.value} < {high.value}")

    def export_serials(self):
        return {
            addr: sstate.serial_last is not None and sstate.serial_last.value
            for addr, sstate in self.servers.items()
        }


class State:
    zones = collections.defaultdict(Zone)
    server_addrs = set()

    def process_notify(self, notify):
        self.server_addrs.add(notify.src)
        zone = self.zones[notify.zone_name]
        zone.record_notify(notify)

    def check(self):
        for name, zone in self.zones.items():
            for err in zone.check():
                print(name, err)

    def export_serials(self):
        fieldnames = ["zone"] + list(sorted(self.server_addrs))

        with open('serials.csv', 'w') as outf:
            writer = csv.DictWriter(outf, fieldnames)
            writer.writeheader()
            for name, zone in self.zones.items():
                columns = zone.export_serials()
                columns["zone"] = name
                writer.writerow(columns)


def loop(state, sock):
    """TODO: minimal demo, should be rewritten to async or whatnot"""
    while True:
        try:
            # dnspython's API is weird - number of return values depends on
            # number of arguments, and we will always get all tree returns
            # pylint: disable=unbalanced-tuple-unpacking
            msg, recvstamp, src = dns.query.receive_udp(sock)
        except KeyboardInterrupt:
            state.check()
            state.export_serials()
            break
        recvtime = datetime.datetime.utcfromtimestamp(recvstamp)
        srcaddr, srcport = src
        addr = ipaddress.ip_address(srcaddr)
        logging.debug("got message from %s:%s on %s: %s", addr, srcport, recvtime, msg)
        notify = Notify(recvtime, addr, msg)
        logging.info("%s", notify)
        state.process_notify(notify)


def main():
    """
    Does not do anything useful yet!
    """
    logging.basicConfig(level=logging.DEBUG)
    state = State()
    parser = argparse.ArgumentParser()
    parser.add_argument("port", nargs="?", default=5300, type=int)
    args = parser.parse_args()
    logging.debug("args: %s", args)

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(("127.0.0.1", args.port))
    loop(state, sock)


if __name__ == "__main__":
    main()
